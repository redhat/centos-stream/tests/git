#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
. /usr/share/beakerlib/beakerlib.sh || exit 1

TEST_USER="test$$"
TEST_DIR="$(pwd)"

user_cleanup(){
    command -v loginctl >/dev/null && rlRun "loginctl terminate-user $TEST_USER" 0,1
    rlRun "pkill -u $TEST_USER" 0,1
    rlRun "userdel -rf $TEST_USER"
}

rlJournalStart
    rlPhaseStartSetup
        rlRun "tmp=\$(mktemp -d)" 0 "Create tmp directory"
        rlRun "set -o pipefail"
        id $TEST_USER &> /dev/null && user_cleanup

        rlRun "useradd $TEST_USER"
        (
            rlRun "cd $tmp"
            rlRun "git init"
            rlRun "git config --global user.email you@example.com"
            rlRun "git config --global user.name 'you you'"
            rlRun "touch this && git add this && git commit -m initial"
        )
        rlRun "chmod a+rX -R $tmp"
    rlPhaseEnd

    rlPhaseStartTest
        rlRun -s "su -l -c 'bash $TEST_DIR/user_script $tmp' $TEST_USER"
        # error message is
        # fatal: detected dubious ownership in repository at '/tmp/foo'
        # To add an exception for this directory, call:
        #   	git config --global --add safe.directory /tmp/foo
        rlAssertGrep 'nothing to commit, working tree clean' $rlRun_LOG
        rlAssertNotGrep 'fatal:' $rlRun_LOG
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "rm -r $tmp" 0 "Remove tmp directory"
        user_cleanup
    rlPhaseEnd
rlJournalEnd
