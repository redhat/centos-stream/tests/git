#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/git/Regression/testsuite
#   Description: Runs the testsuite from SRPM.
#
#   Note: This testcase is based on the former idea of Arjun Shankar,
#   which was to unpack the upstream tarball, build it, test it, then
#   run the same testcases against the git installed from RPM. Then
#   the testsuite compared the results and printed mismatches. It is
#   a good idea generally, but not in this case, because Red Hat ships
#   multiple versions of git and all of them should be possible to be
#   tested by this testsuite. The upstream testsuite contains thousands
#   of testcases and it is very difficult to review the results.
#
#   So that is the main reason why we run the testsuite from matching
#   source RPM to installed git. Despite that, some of the testcases
#   have to be blacklisted to have the Beaker jobs stable and reviewer
#   friendly.
#
#   Authors: Arjun Shankar <ashankar@redhat.com>
#            Michael Petlan <mpetlan@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2013 Red Hat, Inc.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE_FULL_NAME="$(rpm -qf $(which git))"
PACKAGE=`rpm -q --queryformat '%{name}' $PACKAGE_FULL_NAME`

REQUIRES=(gcc glibc-devel glibc-headers wget tar make diffutils patch \
          desktop-file-utils emacs libcurl-devel expat-devel gettext \
          openssl-devel zlib-devel asciidoc xmlto \
          less openssh-clients perl-Error rsync zlib \
          cvs subversion httpd rpmdevtools)

test -z "$GITVER" && GITVER=`rpm -q --queryformat '%{version}' $PACKAGE_FULL_NAME`

SUDO=/usr/bin/sudo # we use this because the dts sudo wrapper is broken

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm --all
        rlRun "which nc" # nc is provided by different packages in RHEL6/7

        rlRun "GITBINARY=$(which git)"
        if [ -z $GITBINARY ]; then
            rlDie "No git available in the PATH"
            rlLog "PATH=$PATH"
        fi

        rlRun "GITDIR=$(dirname $GITBINARY)"

        which git | grep '/opt/rh/'
        if [ $? -eq 0 ]; then
            rlLog "Test will run against collection git: $PACKAGE"
            COLLECTION_NAME=`which git | awk -F '/' '{print $4}'`
            SCL_ENV="scl enable $COLLECTION_NAME" # we will need to re-enable dts
                                                  # when we sudo to non-root user
        else
            rlLog "Test will run against RHEL 'git' package, NOT a collection version."
            SCL_ENV=""
        fi

        # CVS doesn't like running as root. So in order that the CVS
        # testcases pass, we create a user for this test:

# Run a command as user 'gitster', enabling dts if required
function sclrun
{
    if [ -z "$SCL_ENV" ]; then
        $SUDO -u gitster -i sh -c "$1"
    else
        $SUDO -u gitster -i $SCL_ENV "sh -c '$1'"
    fi
}

        rlRun "useradd -m gitster"
        rlRun "sed -i 's/^.*requiretty/#&/' /etc/sudoers"

        rlRun "TmpDir=$($SUDO -u gitster -i mktemp -d)"
        rlRun "TstDir=$(pwd)"

        rlRun "pushd $TmpDir" # this is PWD only for commands not wrapped in sudo

        # Fetch the testsuite matching to the installed git version
        rlFetchSrcForInstalled $PACKAGE

        if yum-builddep --help | grep -- --nobest; then
            NOBEST="--nobest"
        fi
        rlRun "yum-builddep -y $NOBEST *.src.rpm"

        if [ -n "$COLLECTION_NAME" ]; then
            rlRun "yum install -y httpd24-scldevel"
        fi

        rlRun "chmod 666 *.src.rpm" 0 "Making the SRPM package available for gitster"
        rlRun "chown gitster *.src.rpm"
        rlRun "cp $TstDir/*.patch $TmpDir/"
        rlRun "chmod -R o+r $TmpDir"
        rlRun "chmod -R o+w $TmpDir"

        # install the srpm
        rlRun "$SUDO -u gitster -i   \
               sh -c 'cd $TmpDir && \
                      rpm -ivh  *git-*src.rpm'"

        rlRun "SPEC_DIR=$($SUDO -u gitster -i sh -c 'echo $(rpm --eval=%_specdir)')"
        rlRun "SRC_DIR=$($SUDO -u gitster -i sh -c 'echo $(rpm --eval=%_builddir)')/git-$GITVER"

        # build all the sources
        rlRun "echo $SCL_ENV"
        if [ -n "$COLLECTION_NAME" ]; then
            rlRun "sclrun 'cd $SPEC_DIR && rpmbuild -bc --define \"scl $COLLECTION_NAME\" git.spec'"
        else
            rlRun "sclrun 'cd $SPEC_DIR && rpmbuild -bc git.spec'"
        fi
        rlRun "TS_DIR=$SRC_DIR/t" # testsuite directory

        # Find a free ephemeral port for use by svnserve later
        for ssp in {49152..61000}; do
            # If nc succeeds in binding, it will run for the full 5 seconds
            # and be SIGTERM'd by timeout. So we're sure the port is unused.
            timeout 5 nc -l $ssp 2>/dev/null
            if [ $? -eq 124 ]; then
               break
            fi
        done
        rlLog "Port $ssp will be used for svnserve"

    rlPhaseEnd

    rlPhaseStartTest
        # Include a test (#60) that writes location of git binary used
        rlRun "$SUDO -u gitster -i \
               patch -b $TS_DIR/t0000-basic.sh $TmpDir/t0000-basic.patch"
        rlAssertExists "$TS_DIR/t0000-basic.sh.orig"

        my_GIT_TEST_EXEC_PATH=$(dirname $(rpmquery $PACKAGE -l | grep git-init | head -n 1))
        cpus=$(rpm --eval '%{_smp_build_ncpus}')

        # Check that the correct git binary is being used
        rlLog "Now we have to make sure that we use correct binary for testing..."
        rlRun "sclrun 'cd $TS_DIR && \
                       GIT_TEST_INSTALLED=$GITDIR \
                       GIT_TEST_EXEC_PATH=$my_GIT_TEST_EXEC_PATH \
                       PATH=$TS_DIR/helper:\$PATH \
                       ./t0000-basic.sh --verbose 2> /dev/null \
                       | grep -B1 \"ok .* git binary - Fantomas was here\" | head -n 1 \
                       > $TmpDir/bin-test.log'"

        rlAssertEquals "Test must use correct binary" \
                       "$(cat $TmpDir/bin-test.log)" "$GITBINARY"

        # Remove test #60 that we just patched in
        rlRun "$SUDO -u gitster -i \
               mv $TS_DIR/t0000-basic.sh.orig $TS_DIR/t0000-basic.sh"

        # fix permissions @TODO should be nicer/better
        test -e $TS_DIR/t1014-read-tree-confusing.sh && chmod a+x $TS_DIR/t1014-read-tree-confusing.sh

        # run testsuite on installed git
        rlLog "____ RUNNING THE MAIN TESTS ____"
        # note: no rlRun here because it may return whatever exitcode and it still may be PASS
        sclrun "cd $TS_DIR && \
            GIT_TEST_HTTPD=yes \
            GIT_TEST_GIT_DAEMON=yes \
            SVNSERVE_PORT=$ssp \
            GIT_TEST_INSTALLED=$GITDIR \
            GIT_TEST_EXEC_PATH=$my_GIT_TEST_EXEC_PATH \
            PATH=$TS_DIR/helper:\$PATH \
            RPM_BUILD_NCPUS=$cpus \
            make -k &> $TmpDir/installed.testlog"

        rlFileSubmit "installed.testlog" "installed.testlog"

        # Extract aggregate results from runs
        # - 'broken' => known failures that failed as expected
        # - 'fixed' => known failures that passed unexpectedly

        rlLog "Testsuite aggregates for git installed at $GITDIR"
        rlLog "$(cat installed.testlog \
                 | grep -A100 '^make aggregate-results$' \
                 | grep -e '^fixed ' \
                        -e '^success ' \
                        -e '^failed ' \
                        -e '^broken ' \
                        -e '^total ')"
        rlLog "_______ _____"

        # make sure that at least something has run
        rlAssertGrep "t0000-basic.sh" installed.testlog

        rlRun "awk -f $TstDir/filter.awk installed.testlog > installed.faillog"

        rlRun "FAILLOG_LINES_COUNT=$(cat installed.faillog | wc -l)"

        if [ $FAILLOG_LINES_COUNT -ne 0 ]; then
            # let's review the fails and compare them to blacklists
            # the review_fails.pl script compares the faillog on std input with a golden blacklist given as an argument
            # if there are some failures which are not in the blacklist, it will return 1 and print them to stderr
            # there are three blacklists (rhel6, rhel7 and git19 (collection))...

            # select the right blacklist
            if [ -n "$COLLECTION_NAME" ]; then
                # we are running some collection git
                GOLDEN="golden_$COLLECTION_NAME.faillog"
            else
                RHEL_VERSION=`rlGetDistroRelease | awk -F '.' '{print $1;}'`
                GOLDEN="golden_rhel$RHEL_VERSION.faillog"
            fi

            if [ ! -e $TstDir/$GOLDEN ]; then
                GOLDEN=`ls $TstDir/golden* | head -n 1`
                rlLogWarning "Cannot find proper golden file to compare the results with. Using $GOLDEN then."
            fi

            if [ ! -e $TstDir/$GOLDEN ]; then
                rlFail "There are no golden files to compare the fails with. PLEASE CHECK THE FAILLOG MANUALLY."
            fi

            rlLog "Comparing the results with $GOLDEN."
            rlRun "cat installed.faillog | $TstDir/review_fails.pl $TstDir/$GOLDEN"
            rlFileSubmit "installed.faillog" "installed.faillog"
        fi
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
#        mv $TmpDir ./TMP       # debug purposes (uncomment in case you need all the logs)
        rlRun "rm -rf $TmpDir"
        # terminate user (e.g. automated cleanup in systemd user session)
        command -v loginctl >/dev/null && rlRun "loginctl terminate-user gitster" 0,1
        rlRun "userdel -r gitster"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
