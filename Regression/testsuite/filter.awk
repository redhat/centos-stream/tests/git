{
  # Remember the name of the current test script
  if (/^\*\*\*/)
  {
    script=$0;
  }
  # Unexpected failures
  if ((/^not ok/ || /^\*[[:space:]]+FAIL/) && !/\# TODO known breakage$/)
  {
    # Log the current test script name once
    if (script != "X")
    {
      print script;
      script = "X";
    }
    print;
  }
}
