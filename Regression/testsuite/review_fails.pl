#!/usr/bin/perl

die "Usage:\n\tcat your_results.faillog | ./review_fails.pl golden_rhel7.faillog\n" if (($#ARGV < 0) || ($#ARGV > 1));

$golden_file_name = $ARGV[0];
$current_test = "";
%catalog = ();
$fails = 0;

open (INFILE, $golden_file_name) or die "ERROR: THe golden file $golden_file_name does not exist.\n";

# read the desired golden faillog into a hash
while (<INFILE>)
{
	$_ =~ s/\n$//;
	
	# parse current test (group of testcases) name
	if (/^\*\*\*/)
	{
		($a, $current_test, $b) = $_ =~ /^(\*\*\*\s+)(t\d+[-\.\w]+)(\s+\*\*\*)$/;
#		print "I have fount a testset $current_test\n";
		next;
	}
	
	# parse entire failing testcases
	if (/^not ok/)
	{
		($a, $current_testcase) = $_ =~ /^(not ok\s+\d+\s+-\s+)(\S.*)$/;
#		print "    NEW testcase +$current_testcase+\n";
	}
	
	if (/^\* FAIL/)
	{
		($a, $current_testcase) = $_ =~ /^(\*\s+FAIL\s+\d+:\s+)(\S.*)$/;
#		print "    OLD testcase +$current_testcase+\n";
	}
	
	$key_string = $current_test . "###" . $current_testcase;
	$catalog{$key_string} = 1;
}

close INFILE;

#
# now we should parse the stdin and decide which fail is OK and which not
#

while (<STDIN>)
{
	$_ =~ s/\n//;
	
	# parse current test (group of testcases) name
	if (/^\*\*\*/)
	{
		($a, $current_test, $b) = $_ =~ /^(\*\*\*\s+)(t\d+[-\.\w]+)(\s+\*\*\*)$/;
		next;
	}
	
	# parse entire failing testcases
	if (/^not ok/)
	{
		($a, $current_testcase) = $_ =~ /^(not ok\s+\d+\s+-\s+)(\S.*)$/;
	}
	
	if (/^\* FAIL/)
	{
		($a, $current_testcase) = $_ =~ /^(\*\s+FAIL\s+\d+:\s+)(\S.*)$/;
	}
	
	$key_string = $current_test . "###" . $current_testcase;
	
	if (! exists($catalog{$key_string}))
	{
		$fails++;
		print STDERR "UNEXPECTED FAIL: $current_test *** \"$current_testcase\"\n";
	}
}

if ($fails)
{
	print STDERR "\nTOTAL UNEXPECTED FAILS = $fails\n";
	exit 1;
}
else
{
	exit 0;
}
