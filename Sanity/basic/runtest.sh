#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/git/Sanity/basic
#   Description: basic git sanity test
#   Author: Martin Cermak <mcermak@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2013 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="$(rpm -qf $(which git))"

rlJournalStart
    rlPhaseStartSetup
        rlRun "TMPDIR=$(mktemp -d)"
        rlRun "pushd $TMPDIR"

		# only for logging purpose
		which git | grep opt
		if [ $? -eq 0 ]; then
			ACTIVE_COLLECTION="$(which git|awk -F '/' '{print $4}')"
			rlLog "Collection $ACTIVE_COLLECTION is enabled."
		else
			rlLog "No collection for git is enabled."
		fi

		rlLog "We are testing $PACKAGE"
		rlAssertRpm "$PACKAGE"
    rlPhaseEnd

	rlPhaseStartTest "Check dts git binary presence and ability to run."
		rlRun "git --version"
		rlRun "git config --global user.name testuser"
		rlRun "git config --global user.email root@localhost"
		rlRun "git --help | tee help.txt"
		rlRun "git init"
		rlRun "git add help.txt"
		rlRun "git commit -m \"init commit\" help.txt"
		rlRun "git log"
		rlRun "sed -i '/merge/d' help.txt"
		rlRun "git status | tee output.txt"
		rlRun "grep -q 'modified:[[:blank:]]\+help.txt' output.txt"
		rlRun "git diff | tee output.txt"
		rlRun "grep -q '^-[[:blank:]]\+merge' output.txt"
	rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TMPDIR"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
