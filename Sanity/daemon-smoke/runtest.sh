#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/git/Sanity/daemon-smoke
#   Description: starts daemon
#   Author: Lukas Zachar <lzachar@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2015 Red Hat, Inc.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE_FULL_NAME="$(rpm -qf $(which git))"
PACKAGE=`rpm -q --queryformat '%{name}' $PACKAGE_FULL_NAME`

which git | grep opt
if [ $? -eq 0 ]; then
    GIT_BASE_PATH="/var/opt/rh/rh-git29/lib/git"
    SOCKET_PREFIX="rh-git29-"
else
    GIT_BASE_PATH="/var/lib/git"
    SOCKET_PREFIX=""
fi

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
        rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
        rlRun "pushd $TmpDir"

        rlRun "rlFileBackup --clean --missing-ok /etc/gitconfig"

        rlLog "Prepare git in the base path ($GIT_BASE_PATH)"
        pushd $GIT_BASE_PATH
        rlRun "mkdir project"
        cd project
        rlRun "git init"
        rlRun "git config --global user.name 'John Doe'"
        rlRun "git config --global user.email 'john.doe@example.com'"
        rlRun "touch readme; git add readme"
        rlRun "git commit -m 'init'"
        # 'dubious ownership in repository' error otherwise...
        rlRun "chown nobody:nobody -R ."
        popd

        rlLog "Prepare git in the user path (/home/testuser/public_git)"
        rlRun "useradd testuser"
        pushd ~testuser
        rlRun "mkdir -p public_git/userproject"
        rlRun "chmod a+rx ."
        cd public_git/userproject
        rlRun "git init"
        rlRun "touch readme; git add readme"
        rlRun "git commit -m 'init'"
        rlRun "chown -R testuser:testuser ~testuser/public_git"
        # 'dubious ownership in repository' error otherwise...
        rlRun "git config --system --add safe.directory /home/testuser/public_git/userproject/.git"
        rlRun "git config --system --add safe.directory ."
        popd

        rlRun "setsebool -P git_system_enable_homedirs 1"
        rlRun "restorecon -R ~testuser $GIT_BASE_PATH" 0 "Restoring selinux contexts"

        rlLog "Start daemon"
        rlRun "systemctl start ${SOCKET_PREFIX}git.socket"
    rlPhaseEnd

    rlPhaseStartTest
        rlRun "git clone git://127.0.0.1/project"
        rlRun "git clone git://127.0.0.1/~testuser/userproject"
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "systemctl stop git.socket"
        rlRun "userdel -r testuser"
        rlRun "popd"
        rlRun "setsebool -P git_system_enable_homedirs 0"
        rlRun "rm -r $TmpDir $GIT_BASE_PATH/project" 0 "Removing tmp directory"
        rlRun "rlFileRestore"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
