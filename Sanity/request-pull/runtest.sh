#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/git/Sanity/request-pull
#   Description: Basic scenario with git-request-pull
#   Author: Alois Mahdal <amahdal@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2017 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="git"

mkrepo() {
    #
    # Create test repo
    #
    local name=$1
    rlRun "mkdir $name" || {
        rlLogError "error creating repo: $1"
        rlFail "(TEST ERROR)"
        return 3
    }
    rlRun "pushd $name"
        rlRun "git init"

        rlRun "git config user.name 'Alice Original'"
        rlRun "git config user.email 'alice@example.com'"

        rlRun "echo Test repo > README"
        rlRun "git add README"
        rlRun "git commit -m 'Initital commit'"

        rlRun "echo 'echo Hello worls' >run.sh"
        rlRun "git add run.sh"
        rlRun "git commit -m 'Add launcher'"

        rlRun "chmod +x run.sh"
        rlRun "git add run.sh"
        rlRun "git commit -m 'Make launcher executable'"

        rlRun "git tag v0.0.1"

        rlRun "sed -i -e s/s/d/ run.sh"
        rlRun "git add run.sh"
        rlRun "git commit -m 'Fix typo'"

    rlRun "popd"
}

rlJournalStart
    rlPhaseStartSetup
        rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStartSetup "create test repo 'myrepo'"
        mkrepo "myrepo"
    rlPhaseEnd

    rlPhaseStartSetup "create pull request"
        rlRun "git clone file://$PWD/myrepo bobsclone"
        rlRun "pushd bobsclone"

            rlRun "git config user.name 'Bob Useful'"
            rlRun "git config user.email 'bob@example.com'"

            rlRun "echo Cloner was here. >> README"
            rlRun "git add README"
            rlRun "git commit -m 'Make useful change'"

            rlRun "echo echo ..also here. >> run.sh"
            rlRun "git add run.sh"
            rlRun "git commit -m 'Make other useful change'"

            rlRun "git request-pull origin/master file://${PWD} $(git rev-parse HEAD) >../git.out 2>../git.err"
        rlRun "popd"
    rlPhaseEnd

    O_REPOLINK="file://$PWD/bobsclone"
    case $(git --version | cut -d' ' -f3) in
        1.*) O_REPOLINK+=" master" ;;
    esac

    rlPhaseStartTest
        rlRun "test -s git.err" \
            1 "no errors were seen"
        rlRun "test -s git.out" \
            0 "some output was seen"
        rlRun "grep -qF 'Bob Useful (2)' git.out" \
            0 "Bob is mentioned"
        rlRun "grep -qF 'Make useful change' git.out" \
            0 "First commit is mentioned"
        rlRun "grep -qF 'Make other useful change' git.out" \
            0 "Second commit is mentioned"
        rlRun "grep -qF '2 files changed, 2 insertions(+)' git.out" \
            0 "Commit summmary is correct"
        rlRun "grep -qF '$O_REPOLINK' git.out" \
            0 "Commit summmary is correct"
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
